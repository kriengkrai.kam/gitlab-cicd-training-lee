# LAB Basic gitlab-ci.yml

## Content
- Create .gitlab-ci.yml
- Custom .gitlab-ci.yml
- Set and Use variable

## Create `.gitlab-ci.yml`
1. Create project in gitlab or Open your project
2. Edit with WebIDE
3. Create `.gitlab-ci.yml`
   - Copy following file:
        ``` yaml
        stages:
        - test
        
        job:
          stage: test
          tags:
            - <runner>
          script:
            - echo "test"
        ```
4. Change <runner> to your runner tag
5. Cimmit change
6. See the pipeline running

## Custom `.gitlab-ci.yml`
1. Create Parallel job
    ``` yaml
    stages:
    - test

    job1:
     stage: test
     tags:
     - runner01
     script:
     - echo "job1"
        
    job2:
     stage: test
     tags:
     - runner01
     script:
     - echo "job2"
    ```
2. Create more stage
    ``` yaml
    stages:
    - test
    - deploy

    job1:
     stage: test
     tags:
     - runner01
     script:
     - echo "test"

    job2:
     stage: deploy
     tags:
     - runner01
     script:
     - echo "deploy"
    ```
3. Create environment variable
    ``` yaml
    variables:
      ENV: prd
    stages:
    - test
    - deploy

    job1:
     stage: test
     tags:
     - runner01
     script:
     - echo $ENV
 
    job2:
     stage: deploy
     tags:
     - runner01
     script:
     - echo $ENV
    ```
4. Define environment for deploy job
    ``` yaml
    stages:
    - build
    - test
    - deploy

    build:
     stage: build
     tags:
     - runner01
     variables:
       ENV: test
     script:
     - echo "Build app here"
    
    test:
     stage: test
     tags:
     - runner01
     variables:
       ENV: test
     script:
     - echo "Test app here"
    
    deploy_dev:
     stage: deploy
     tags:
     - runner01
     variables:
       ENV: test
     script:
     - echo "deploy app on $ENV"
     environment: staging

    deploy_prd:
     stage: deploy
     tags:
     - runner01
     variables:
      ENV: prd
     script:
     - echo "deploy app on $ENV"
     environment: production
    ```
## Set and Use variable
1. Open peoject setting > CICD

<img src="image/settingcicd.png" alt="drawing" width="400"/>

2. Expand Variable

<img src="image/expand_variable.png" alt="drawing" width="400"/>

3. Add 2 Variable 
   - ENV 

<img src="image/AddENV.png" alt="drawing" width="400"/>

   - PASSWORD with checking Mask variableb 

<img src="image/AddPASSWORD.png" alt="drawing" width="400"/>

4. Test in pipeline
    ``` yaml
    stages:
    - test

    job1:
     stage: test
     tags:
     - runner01
     script:
     - echo $ENV
     - echo $PASSWORD
    ```
5. Create same name variable in `.gitlab-ci.yml`
    ``` yaml
    variables:
        ENV: production
    stages:
    - test

    job1:
     stage: test
     tags:
     - runner01
     script:
     - echo $ENV
     - echo $PASSWORD
    ```
6. Create variable in job1
    ``` yaml
    variables:
      ENV: production
    stages:
        - test

    job1:
     stage: test
     tags:
     - runner01
     variables:
       ENV: test
     script:
     - echo $ENV
     - echo $PASSWORD
    ```
7. Create job2 with no job variables
    ``` yaml
    variables:
      ENV: production
    stages:
    - test

    job1:
      stage: test
      tags:
      - runner01
      variables:
        ENV: test
      script:
      - echo $ENV
      - echo $PASSWORD

    job2:
      stage: test
      tags:
      - runner01
      script:
      - echo $ENV
      - echo $PASSWORD
    ```
8. Checkout [predefined variable](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) and try to use it
    ``` yaml
    variables:
      ENV: production
    stages:
    - test

    job1:
     stage: test
     tags:
     - runner01
     variables:
       ENV: test
     script:
     - echo "This branch is $CI_COMMMIT_BRANCH"
    ```


